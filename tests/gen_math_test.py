import sys
sys.path.append('classes/')

from gen_math import MathEquation
from fpdf import FPDF
from pprint import pprint


def to_pdf(eqns: list):

    # equations = ',\n'.join(eqns)
    pdf = FPDF(orientation='P', unit='mm', format='A5')
    pdf.add_page()
    pdf.set_font('Arial', 'B', 20)
    for item in eqns:
        pdf.cell(40, 15, txt=item, border=0, ln=1)
    pdf.output('tuto1.pdf', 'F')


m = MathEquation()

eqns = m.gen_equation(total_number=20)

# pprint(eqns)
to_pdf(eqns)
print('Done!')
