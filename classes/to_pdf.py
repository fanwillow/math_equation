from fpdf import FPDF


class PDF(FPDF):

    def footer(self):
        """ makes pager footer. """
        # position at 1.5cm from bottom
        self.set_y(-15)
        # font: Arial, Italic, 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')


def to_A4_L(eqn_A: list, eqn_B: list, out_file: str):
    """ save [eqn_A] to "out_file" with format A4, Landscape. """

    # equations = ',\n'.join(eqns)
    pdf = PDF(orientation='L', unit='mm', format='A4')
    pdf.add_page()

    pdf.set_font('Arial', 'B', 16)
    pdf.set_title("Emily's assignment: ")

    pdf.cell(50, 15, txt="Emily's assignment: ", ln=1)
    for i in range(0, len(eqn_A), 2):
        pdf.cell(40, 15, txt=eqn_A[i], border=0, ln=0)
        pdf.cell(30, 15, txt='', border=0, ln=0)
        pdf.cell(40, 15, txt=eqn_A[i+1], border=0, ln=0)

        pdf.cell(40, 15, txt='', border=0, ln=0)

        pdf.cell(40, 15, txt=eqn_B[i], border=0, ln=0)
        pdf.cell(30, 15, txt='', border=0, ln=0)
        pdf.cell(40, 15, txt=eqn_A[i+1], border=0, ln=1)


    pdf.dashed_line(145, 10, 145, 200, 1, 1)

    pdf.output(out_file, 'F')
    print('Done! ')


def to_A5_P(eqns: list, out_file: str):
    """ save [eqns] to "out_file" with format A5, Portrait. """
    pdf = PDF(orientation='P', unit='mm', format='A5')
    pdf.alias_nb_pages()
    pdf.add_page()

    pdf.set_font('Arial', 'B', 16)
    pdf.set_title("Emily's assignment: ")

    pdf.cell(50, 15, txt="Emily's assignment: ", ln=1)
    for i in range(0, len(eqns), 2):
        pdf.cell(40, 15, txt=eqns[i], border=0, ln=0)
        pdf.cell(30, 15, txt='', border=0, ln=0)
        pdf.cell(40, 15, txt=eqns[i+1], border=0, ln=1)

    pdf.output(out_file, 'F')
    print(f'Done! {out_file} saved. ')