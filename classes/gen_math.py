"""
This module generates mathematics equations for my kids
 > Addition "A + B = C"
    + eqn_AB: "A + B = ?"
    + eqn_AC: "A + ? = C"
    + eqn_BC: "? + B = C"

 > Subtraction

 > Multiplication

 > Division

"""
import numpy as np
import random


class Addition:


    def eqn_AB(self, a: int, b: int) -> str:
        """ return a string 'A + B = __' """
        return f'{a}  +  {b}  =  ___ '

    def eqn_AC(self, a: int, b: int) -> str:
        """ return a string 'A + ___ = C' """
        return f'{a}  +  ___  =  {a + b} '

    def eqn_BC(self, a: int, b: int) -> str:
        """ return a string 'A + B = __' """
        return f'___  +  {b}  =  {a + b} '

    def generate_AB(self, row: int, max_int: int = 10) -> list:
        """Generate a list of addends A and B. """
        random.seed()

        addents = []
        for i in range(row):
            t_ = []
            for j in range(2):
                t_.append(random.randrange(2, max_int))
            addents.append(t_)

        return addents

    def make_equations(self, addents: list, groups: list) -> list:
        """ Make equations.

         Arguments:
             addents {list} -- a list of addents A and B
             groups {list} -- a list of integer nubmer [m, n, l], m+n+l= len(addents)

         Returns:
             {list} -- a list of equations including eqn_AB, eqn_AC, eqn_BC
         """
        cnt = len(addents)
        eqn = []

        if sum(groups) != cnt:
            goups[2] = cnt - groups[0] - groups[1]

        # eqn_AB:
        for i in range(groups[0]):
            x = addents[i][0]
            y = addents[i][1]
            eqn.append(self.eqn_AB(x, y))

        # eqn_AC:
        if groups[1]>0:
            for j in range(groups[0], groups[0] + groups[1], 1):
                x = addents[j][0]
                y = addents[j][1]
                eqn.append(self.eqn_AC(x, y))

        # eqn_BC:
        if groups[2]>0:
            for k in range(groups[0] + groups[1], cnt, 1):
                x = addents[k][0]
                y = addents[k][1]
                eqn.append(self.eqn_BC(x, y))

        random.shuffle(eqn)
        return eqn


# not used . ############################################################
    @staticmethod
    def gen_equation(total_number=10) -> list:
        random.seed()

        equations = []
        # total_number = 10

        for i in range(total_number):
            a1 = random.randrange(10)
            a2 = random.randrange(10)

            if a1 + a2 != 0:
                eqn = f'{a1} + {a2} =    '
                equations.append(eqn)

        is_ready = False
        while not is_ready:
            dif = len(equations) - len(set(equations))
            if dif:
                tmp = list(set(equations))
                random.seed()
                for i in range(dif):
                    a1 = random.randrange(10)
                    a2 = random.randrange(10)

                    if a1 + a2 != 0:
                        eqn = f'{a1} + {a2} =  '
                        tmp.append(eqn)
                equations = tmp
            else:
                is_ready = True

        return equations

    def generate_terms0(self, row: int, column: int=2, max_int: int=10) -> list:
        """ Generate terms used for creating arithmetic equations.

        Arguments:
            number {int} -- number of terms to be generated, default=2

        Returns:
            {list} -- a list of integer numbers
        """
        random.seed()
        t_ = np.zeros((row, column), dtype=np.int16)

        for i in range(row):
            for j in range(column):
                t_[i][j] = random.randrange(max_int)

        terms = []
        for i in range(row):
            l_ = ', '.join(str(x) for x in t_[i])
            terms.append(l_)

        return terms



