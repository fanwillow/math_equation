
from classes.gen_math import Addition
from classes.to_pdf import *
from pprint import pprint
from pathlib import Path
import random


# def test():
if __name__ == '__main__':
    m = Addition()

    eqn_count = 20
    max_number = 30
    addents = m.generate_AB(eqn_count, max_number)

    sets = [10, 6, 4]    # sum of sets = eqn_number
    eqnA = m.make_equations(addents, sets)
    sets = [10, 6, 4]
    eqnB = m.make_equations(addents, sets)

    des = 'a5'
    if des == 'a4':
        out_f = Path(r'demo/out1.pdf')
        to_A4_L(eqnA, eqnB, out_f.as_posix())
    elif des == 'a5':
        out_f = Path(r'demo/out2.pdf')
        to_A5_P(eqnA + eqnB, out_f)

# def main():
#     m = MathEquation()
#
#     eqns = m.gen_equation(total_number=20)
#
#     home_folder = Path.cwd().home()
#     outf = home_folder / r'temp.dir/out.pdf'
#     if not outf.parent.exists():
#         outf.parent.mkdir(parents=True)
#
#     to_pdf(eqns, outf.as_posix())
#     print('Done!')



# if __name__ == '__main__':
#     test()